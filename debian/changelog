python-django-simple-history (3.7.0-2) unstable; urgency=medium

  * Drop dependency on python3-pkg-resources.
    This runtime dependency has been already dropped upstream.
    (Closes: #1083628)

 -- Antonio Terceiro <terceiro@debian.org>  Fri, 04 Oct 2024 07:56:23 -0300

python-django-simple-history (3.7.0-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Set field Upstream-Contact in debian/copyright.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-Browse.
  * Remove obsolete field Contact from debian/upstream/metadata (already present
    in machine-readable debian/copyright).
  * Update standards version to 4.6.2, no changes needed.

  [ Antonio Terceiro ]
  * fix debian/watch to work against new upstream release style
  * New upstream version 3.7.0
  * Add patch to skip unecessary metadata hook in pyproject.toml
  * Run tests during build and autokpgtest
  * Build using the pybuild pyproject plugin and hatchling
  * Bump Standards-Version to 4.7.0; no changes needed

 -- Antonio Terceiro <terceiro@debian.org>  Tue, 20 Aug 2024 15:18:13 -0300

python-django-simple-history (3.1.1-1) unstable; urgency=medium

  * New upstream version 3.1.1
  * Add build dependency on python3-setuptools-scm
  * debian/watch: bump to version 4
  * Bump Standards-Version to 4.6.1; no changes required
  * Run upstream test suite under autopkgtest
  * Add runtime dependency on python3-pkg-resources

 -- Antonio Terceiro <terceiro@debian.org>  Thu, 04 Aug 2022 19:53:38 -0300

python-django-simple-history (2.7.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.0.
  * Bump Standards-Version to 4.4.1.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Bump debhelper from old 12 to 13.
  * Set field Upstream-Name in debian/copyright.
  * Set upstream metadata fields: Contact.
  * Update standards version to 4.5.1, no changes needed.

 -- Sandro Tosi <morph@debian.org>  Wed, 08 Jun 2022 21:57:20 -0400

python-django-simple-history (2.7.0-1) unstable; urgency=medium

  * Initial release

 -- Antonio Terceiro <terceiro@debian.org>  Wed, 23 Jan 2019 11:13:08 -0200
